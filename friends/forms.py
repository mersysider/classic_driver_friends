from django.contrib.auth.forms import UserCreationForm 
# a modelForm
class MyRegistrationForm(UserCreationForm):
	from django import forms
	password1 = forms.CharField(max_length=32, widget=forms.PasswordInput)
	password2 = forms.CharField(max_length=32, widget=forms.PasswordInput)
	class Meta:
		# this adds an extra bit of information
		# define anything that isnt actually a form field
		from django.contrib.auth.models import User
		model = User
		fields = ('username','password1','password2','first_name','last_name')

from django.contrib.gis import forms
class MyLocationForm(forms.ModelForm):
	class Meta:
		from .models import Location
		model = Location
		fields = '__all__'
