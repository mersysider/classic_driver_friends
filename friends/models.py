from __future__ import unicode_literals
from django.contrib.gis.db import models
# from django.db import models
from django.contrib.gis import forms
# Create your models here.

# class Friend_List(models.Model):
# 	name = models.CharField(max_length=200)
# 	location = models.PointField(null=True)
# 	fren = models.BooleanField(default=False)

# 	def __unicode__(self):
# 		return self.name

class Users(models.Model):
	# model of the provider
	username = models.CharField(max_length=200)
	first_name = models.CharField(max_length=100)
	last_name = models.CharField(max_length=100)

	# point = models.PointField(null=True)
	# friend_list = models.ForeignKey(Friend_List, models.SET_NULL, null=True, blank=True)
	# point = forms.PointField(widget=forms.OSMWidget(attrs={'map_width': 800, 'map_height': 500}))
	# objects = models.GeoManager()

	def __unicode__(self):
		return self.name

class Location(models.Model):
	location = models.PointField(null=True)