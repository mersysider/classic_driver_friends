from django.conf.urls import url
from .views import all_users, register_user, profile

urlpatterns = [

    url(r'^all/', all_users , name='all-users'), 
    url(r'^register/', register_user , name='register-user'), 
    url(r'^get/(?P<user_id>\d+)/$',profile),
   ]