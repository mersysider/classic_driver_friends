from django.shortcuts import render, render_to_response
# from .models import Users
# from .forms import UsersForm, MyGeoForm
from .forms import MyRegistrationForm
from django.template.context_processors import csrf
from django.contrib.gis.db.models.functions import *
from django.http import HttpResponseRedirect
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.measure import D 
from django.contrib.auth.models import User

# Create your views here.

def all_users(request):
	# pagination required
	'''this function shows all friends page'''
	return render_to_response('users.html',{'users':User.objects.all()})


def register_user(request):
	if request.method == "POST":
		# form = UserCreationForm(request.POST)
		form = MyRegistrationForm(request.POST)
		if form.is_valid():
			# validates
			print "===================FORM========================"
			print form
			form.save()
			print "===================WORKED========================"
			return HttpResponseRedirect('/users/all')
	
	args = {}
	args.update(csrf(request))
	args['form'] = MyRegistrationForm()

	# a blank user creation form, has NO INFO put in it, NO DATA
	return render_to_response('register.html', args)

def profile(request,user_id=1):
	return render_to_response('profile.html',{'profile': User.objects.get(id=user_id)})